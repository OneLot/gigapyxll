import requests
import datetime
import time
import typing
import threading
import logging
from pyxll import RTD, xl_func
import sys

_log = logging.getLogger(__name__)


class BookOrder(object):
    def __init__(self, bids, asks):
        self.bids = bids
        self.asks = asks
        self.time_stamp = datetime.datetime.now()

    def __eq__(self, other):
        if other == 0:
            return False
        return self.bids == other.bids and self.asks == other.asks

    def __str__(self):
        return (str(self.asks[4]) + "\n" +
                str(self.asks[3]) + "\n" +
                str(self.asks[2]) + "\n" +
                str(self.asks[1]) + "\n" +
                str(self.asks[0]) + "\n\n\n" +
                str(self.bids[0]) + "\n" +
                str(self.bids[1]) + "\n" +
                str(self.bids[2]) + "\n" +
                str(self.bids[3]) + "\n" +
                str(self.bids[4]))



class BitstampClient:
    def __init__(self):
        self._client = requests.Session()
        self._book_order: typing.Optional[BookOrder] = None

    def get_data(self, currency_pair: str):
        return self._client.get(f"https://www.bitstamp.net/api/v2/ticker/{currency_pair}").content

    def get_book_order(self, currency_pair: str):
        book_order_json = self._client.get(f"https://www.bitstamp.net/api/v2/order_book/" + currency_pair + "/").json()
        return BookOrder(
            bids=book_order_json['bids'][5:],
            asks=book_order_json['asks'][5:])


class LastBookOrderEthereumRTD(RTD):
    """
    periodically updates its value with the last price.
    Whenever the value is updated Excel is notified
    and when Excel refreshes the new value will be displayed.
    """

    def __init__(self, currency_pair):
        initial_value = 0
        super(LastBookOrderEthereumRTD, self).__init__(value=initial_value)
        self.client = BitstampClient()
        self.currency_pair = currency_pair
        self.__running = True
        self.__thread = threading.Thread(target=self.__thread_func)
        self.__thread.start()

    @staticmethod
    def connect():
        _log.info("SymbolLastPrice Connected")

    def disconnect(self):
        self.__running = False
        _log.info("SymbolLastPrice Disconnected")

    def __thread_func(self):
        while self.__running:
            try:
                book = self.client.get_book_order(self.currency_pair)
                if str(self.value) != str(book):
                    self.value = str(book)
            except Exception as e:
                _log.error("Error setting RTD value", exc_info=True)
                exc_type, exc_value, exc_trace = sys.exc_info()
                self.set_error(exc_type, exc_value, exc_trace)

            time.sleep(0.2)


@xl_func("var currency_pair: rtd", recalc_on_open=True)
def get_book_order(currency_pair):
    return LastBookOrderEthereumRTD(currency_pair)
