from typing import Tuple, Dict, List, Optional
import bs4


class Bond(object):
    def __init__(self,
                 yield_pct,
                 duration_label,
                 duration_months,
                 country_name,
                 url):
        self.yield_pct = yield_pct
        self.duration_label = duration_label
        self.duration_months = duration_months
        self.url = url
        self.country = country_name
