#!/usr/bin/env python

import re
import time
import bs4
import json
import datetime
import pandas as pd
from time import strptime
from dateutil import relativedelta


class Symbol(object):
    def __init__(self, name, info, url, client, sym_type='equity'):
        self.name = name
        self.info = info
        self.sym_type = sym_type
        self.url = url
        self._client = client
        self._pair_id = False
        self._maturity_url = False
        self._strikes_url = False

    def _get(self):
        r = self._client.get(self.url, timeout=20)
        r.raise_for_status()
        return r

    def get_pairid(self):
        if not self._pair_id:
            r = self._get()
            symbol_soup = bs4.BeautifulSoup(r.content, 'lxml')
            pair_id = symbol_soup.find('div', attrs={'data-pair-id': True})
            self._pair_id = pair_id['data-pair-id']

        return self._pair_id

    def _get_strikes_url(self, maturity_unix):
        self.get_pairid()
        strikes_url = 'https://www.investing.com/instruments/OptionsDataAjax?pair_id={1}&date={0}&strike=all&callsputs=call_and_puts&type=quotes&bringData=true'.format(maturity_unix, self._pair_id)
        return self._client.get(strikes_url)

    def _get_option_maturities_and_spot(self):
        self.get_pairid()
        self._maturity_url = self.url + "-options"
        r = self._client.get(self._maturity_url)
        if r.status_code == 404:
            return None, None
        soup = bs4.BeautifulSoup(r.content, 'lxml')
        spot_text = soup.find('span', id='last_last').text
        spot = float(spot_text.replace(",", ""))
        options_expy_list = soup.find('select', id="selectDate")
        expiry_unix_by_datetime = {}
        for each in options_expy_list.find_all('option'):
            dateArr = each.text.split(" ")
            dateArr[1] = dateArr[1].lstrip('0').rstrip(',')
            unixTimeStamp = each.attrs["value"]
            dateTime = datetime.date(int(dateArr[2]), strptime(dateArr[0], '%b').tm_mon, int(dateArr[1]))
            expiry_unix_by_datetime[dateTime] = unixTimeStamp
        return expiry_unix_by_datetime, spot

    def _option_dictionary_by_market_line(self, market_data):
        #https://en.wikipedia.org/wiki/Option_symbol
        symbol_split = market_data[0].find_all('span')[1].text.split("|")
        if len(symbol_split) > 1:
            strike = symbol_split[2][:-1]
            if 'C' in symbol_split[2][-1:]:
                optionType = 'call'
            elif 'P' in symbol_split[2][-1:]:
                optionType = 'put'
            else:
                optionType = 'unknown - check your soup'
        else:
            strike = (market_data[0].find_all('span')[1].text[-8:][:-1]).lstrip('0')
            if 'C' in market_data[0].find_all('span')[1].text:
                optionType = 'call'
            elif 'P' in market_data[0].find_all('span')[1].text:
                optionType = 'put'
            else:
                optionType = 'unknown - check your soup'
        thisdict = {'type': optionType,
                    'symbol': ",".join(symbol_split),
                    'strike': strike,
                    'delta': market_data[1].find_all('span')[1].text,
                    'implied_vol': market_data[2].find_all('span')[1].text,
                    'bid': market_data[3].find_all('span')[1].text,
                    'gamma': market_data[4].find_all('span')[1].text,
                    'theoretical': market_data[5].find_all('span')[1].text,
                    'ask': market_data[6].find_all('span')[1].text,
                    'theta': market_data[7].find_all('span')[1].text,
                    'instrinsic_value': market_data[8].find_all('span')[1].text,
                    'volume': market_data[9].find_all('span')[1].text,
                    'vega': market_data[10].find_all('span')[1].text,
                    'time_value': market_data[11].find_all('span')[1].text,
                    'openint': market_data[12].find_all('span')[1].text,
                    'rho': market_data[13].find_all('span')[1].text
                    }
        return thisdict

    def _get_options_df(self, maturity_unix, strikes, spot):
        r = self._get_strikes_url(maturity_unix)
        data = json.loads(r.content)
        soup = bs4.BeautifulSoup(data['data'], 'lxml')
        nearest_expiry_table = soup.find(id="optionsData")
        all_options = []

        for option in nearest_expiry_table('div', {'class': "clear overviewDataTable"}):
            market_data = option.find_all('div', {'class': "inlineblock"})
            option_dictionary = self._option_dictionary_by_market_line(market_data)
            all_options.append(option_dictionary)

        pd.options.mode.chained_assignment = None  # default='warn'
        df = pd.DataFrame(columns=['ask', 'bid', 'delta', 'openint', 'strike', 'type', 'volume'])

        for option in all_options:
            df = df.append(pd.DataFrame({'bid': option['bid'], 'ask': option['ask'], 'delta': option['delta'],
                                         'openint': option['openint'], 'strike': option['strike'], 'type': option['type'],
                                         'gamma': option['gamma'], 'theo': option['theoretical'], 'volume': option['volume'],
                                         'theta': option['theta'], 'instrinsic_value': option['instrinsic_value'],
                                         'vega': option['vega'], 'time_value': option['time_value'], 'rho': option['rho']
                                         }, index=[0]), ignore_index=True, sort=True)

        df_call = df.loc[df['type'] == 'call']
        df_call.set_index('strike', inplace=True)
        df_call.rename(index=str,
                       columns={'bid': 'call_bid', 'ask': 'call_ask', 'delta': 'call_delta', 'openint': 'call_openint',
                                'strike': 'call_strike', 'type': 'call_type', 'volume': 'call_volume',
                                'theta': 'call_theta', 'instrinsic_value': 'call_instrinsic_value',
                                'vega': 'call_vega', 'time_value': 'call_time_value', 'rho': 'call_rho',
                                'gamma': 'call_gamma', 'theo': 'call_theo'}, inplace=True)

        df_put = df.loc[df['type'] == 'put']
        df_put.set_index('strike', inplace=True)
        df_put.rename(index=str,
                      columns={'bid': 'put_bid', 'ask': 'put_ask', 'delta': 'put_delta', 'openint': 'put_openint',
                                'strike': 'put_strike', 'type': 'put_type', 'volume': 'put_volume',
                                'theta': 'put_theta', 'instrinsic_value': 'put_instrinsic_value',
                                'vega': 'put_vega', 'time_value': 'put_time_value', 'rho':'put_rho',
                                'gamma': 'put_gamma', 'theo': 'put_theo'}, inplace=True)

        options_straddle_DF = pd.concat([df_call, df_put], axis=1)
        options_straddle_DF.reset_index(inplace=True)
        options_straddle_DF = options_straddle_DF[
            ['call_bid', 'call_ask', 'call_delta', 'call_openint', 'call_volume','call_theta', 'call_instrinsic_value',
             'call_vega', 'call_time_value', 'call_rho', 'call_gamma', 'call_theo',
             'strike',
             'put_bid', 'put_ask', 'put_delta', 'put_openint', 'put_volume', 'put_theta', 'put_instrinsic_value',
             'put_vega', 'put_time_value', 'put_rho', 'put_gamma', 'put_theo']]

        #there might be commas in some stupid fields
        for field in ['call_bid', 'call_ask', 'put_bid', 'put_ask', 'strike']:
            options_straddle_DF[field] = options_straddle_DF[field].apply(lambda x: x.replace(',', '') if ',' in x else x)

        options_straddle_DF['call_bid'] = pd.to_numeric(options_straddle_DF.call_bid)
        options_straddle_DF['call_ask'] = pd.to_numeric(options_straddle_DF.call_ask)
        options_straddle_DF['call_delta'] = pd.to_numeric(options_straddle_DF.call_delta).round(decimals=2)
        options_straddle_DF['strike'] = options_straddle_DF['strike'].apply(lambda x: x.replace(',', '') if ',' in x else x)
        options_straddle_DF['strike'] = pd.to_numeric(options_straddle_DF.strike, downcast='signed')
        options_straddle_DF['put_bid'] = pd.to_numeric(options_straddle_DF.put_bid)
        options_straddle_DF['put_ask'] = pd.to_numeric(options_straddle_DF.put_ask)
        options_straddle_DF['put_delta'] = pd.to_numeric(options_straddle_DF.put_delta).round(decimals=2)

        mid_strike_len = len(options_straddle_DF.strike) // 2
        if options_straddle_DF.strike.loc[mid_strike_len] > (75 * spot):
            options_straddle_DF['strike'] = options_straddle_DF['strike'] / 100

        nearOTM_row = options_straddle_DF[options_straddle_DF.strike > spot].index[0]
        return (options_straddle_DF.loc[(options_straddle_DF.index > (nearOTM_row - strikes - 1)) &
                                       (options_straddle_DF.index < (nearOTM_row + strikes))],
                int(maturity_unix))

    def _get_expiry_epoch_by_dico_and_dto(self, time_dto, expiry_dictionary):
        """
        :param time_dto: weekly or month dto pass from get_options
        :param expiry_dictionary: a dico of this symbol option expiry with key datetime, value epoch time
        :return: the epoch time to query investing endpoint
        """
        time_dto = time_dto.lower()
        unix_list = list(expiry_dictionary.values())
        expiry_datetime_list = list(expiry_dictionary.keys())
        now = datetime.date.today()

        if time_dto == "fd1" or time_dto is False:
            return unix_list[0]
        elif time_dto == "fd2":
            return unix_list[1]
        elif time_dto == "fd3":
            return unix_list[2]
        elif time_dto[0] == "m" and 12 >= int(time_dto[1]) >= 2:
            relative_month = (now + relativedelta.relativedelta(months=int(time_dto[1])))
            descending_filtered_datetime = [x for x in expiry_datetime_list if (x.month == relative_month.month and
                                                                                 x.year == relative_month.year)]
            if len(descending_filtered_datetime) > 0:
                return expiry_dictionary[descending_filtered_datetime[-1]]
        elif time_dto == "m0":
            descending_filtered_datetime = [x for x in expiry_datetime_list if (x.month == now.month and
                                                                                 x.year == now.year)]
            if len(descending_filtered_datetime) > 1:
                return expiry_dictionary[descending_filtered_datetime[-2]]
            elif len(descending_filtered_datetime) > 0:
                return expiry_dictionary[descending_filtered_datetime[-1]]
            else:
                return
        elif time_dto == "m1":
            relative_month = (now + relativedelta.relativedelta(months=1))
            descending_filtered_datetime = [x for x in expiry_datetime_list if (x.month == relative_month.month and
                                                                                 x.year == relative_month.year)]
            if len(descending_filtered_datetime) >= 5:
                return expiry_dictionary[descending_filtered_datetime[-2]]
            elif 4 >= len(descending_filtered_datetime) > 0:
                return expiry_dictionary[descending_filtered_datetime[-1]]
            else:
                return
        return

    def get_options(self, time_dto="fd1", strikes=5):
        ''''
        :param time_dto: 'fd1' nearest expiry, 'fd2' second, 'fd3' third, 'm0' current month last expy,
        'm1' last of next month, 'm2' last of month + 2, 'm3' last of month + 3.
        :param strikes: 'amount of strike above and below at the money to include'
        :return: 'a tuple of pandas dataframe and epoch maturity',
                 can be return null, null if expired or dto is not listed
        '''
        expiry_dict, spot = self._get_option_maturities_and_spot()
        if expiry_dict is not None:
            maturity_unix = self._get_expiry_epoch_by_dico_and_dto(time_dto, expiry_dict)
            if maturity_unix is not None:
                return self._get_options_df(maturity_unix, strikes, spot)
        return None, None

    def get_data_series(self, resolution='15', lookback=50, to_time=None):
        # Turn string values into integers for the lookback window
        string_maps = {
            'D': 1440,
            'W': 1440 * 7,
            'M': 1440 * 7 * 31,
        }
        if resolution in string_maps.keys():
            lookback_resolution = string_maps[resolution]
        else:
            lookback_resolution = int(resolution)
        # calculate time and offset for lookback
        to_time = to_time if to_time else int(time.time())
        from_time = to_time - ((lookback + 2) * lookback_resolution * 60)

        # get pair id
        pair_id = self.get_pairid()

        args = {
            'to_time': to_time,
            'from_time': from_time,
            'pair_id': pair_id,
            'resolution': resolution,
        }

        url = "https://tvc4.forexpros.com/87ec66a24d0f0f35df500ff41071333e/%(to_time)s/1/1/7/history?symbol=%(pair_id)s&resolution=%(resolution)s&from=%(from_time)s&to=%(to_time)s" % args
        r = self._client.get(url)
        return r.content

    def quote(self):
        r = self._get()
        q = {}
        symbol_soup = bs4.BeautifulSoup(r.content, 'lxml')
        q['last'] = symbol_soup.find('span', id='last_last').contents[0]
        q['changed'] = q['last'].find_next('span', dir='ltr').contents[0]
        q['changed_pct'] = q['changed'].find_next(
            'span', dir='ltr').contents[0]

        ext_block = symbol_soup.find('div', class_='afterHoursInfo')

        if ext_block and re.search(r'After', ext_block.contents[0]):
            q['ah_last'] = ext_block.find(
                'span', class_=re.compile(r'pidExt-\d+-last')).contents[0]
            q['ah_changed'] = ext_block.find(
                'div', class_=re.compile(r'pidExt-\d+-pc')).contents[0]
            q['ah_changed_pct'] = ext_block.find(
                'div', class_=re.compile(r'pidExt-\d+-pcp')).contents[0]

        if ext_block and re.search(r'Pre', ext_block.contents[0]):
            q['pm_last'] = ext_block.find(
                'span', class_=re.compile(r'pidExt-\d+-last')).contents[0]
            q['pm_changed'] = ext_block.find(
                'div', class_=re.compile(r'pidExt-\d+-pc')).contents[0]
            q['pm_changed_pct'] = ext_block.find(
                'div', class_=re.compile(r'pidExt-\d+-pcp')).contents[0]

        return q

    def overview(self):
        r = self._get()
        symbol_soup = bs4.BeautifulSoup(r.content, 'lxml')
        overview = symbol_soup.find(
            'div',
            class_='clear overviewDataTable overviewDataTableWithTooltip')
        overview_boxes = overview.find_all('div', recurse=False)

        overview_info = {}
        for box in overview_boxes:
            label = box.contents[0].contents[0]
            value = box.contents[1].contents[0]

            # Sometimes a field will jam a url inside.  Strip out the text
            if box.contents[1].find('a'):
                value = value.contents[0]

            overview_info[label] = value
        return overview_info


if __name__ == '__main__':
    pass
