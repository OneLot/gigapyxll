from src.pyinvesting import Client
from pyxll import RTD, xl_func
import threading
import logging
import time
import sys


_log = logging.getLogger(__name__)


class LastStockPriceRTD(RTD):
    """
    periodically updates its value with the last price.
    Whenever the value is updated Excel is notified
    and when Excel refreshes the new value will be displayed.
    """

    def __init__(self, symbol):
        initial_value = 0
        super(LastStockPriceRTD, self).__init__(value=initial_value)
        self.client = Client()
        self._symbol = symbol
        self.__running = True
        self.__thread = threading.Thread(target=self.__thread_func)
        self.__thread.start()

    def connect(self):
        _log.info("SymbolLastPrice Connected")

    def disconnect(self):
        self.__running = False
        _log.info("SymbolLastPrice Disconnected")

    def __thread_func(self):
        while self.__running:
            try:
                s = self.client.get_symbol(self._symbol)
                last = s.quote()['last']
                if self.value != last:
                    self.value = last
            except:
                _log.error("Error setting RTD value", exc_info=True)

                # Report the error back to Excel
                exc_type, exc_value, exc_trace = sys.exc_info()
                self.set_error(exc_type, exc_value, exc_trace)

            time.sleep(30)


@xl_func("var symbol: rtd", recalc_on_open=True)
def get_stock_last_rtd(symbol):
    return LastStockPriceRTD(symbol)
