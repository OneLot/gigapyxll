from pyxll import xl_func
from py_vollib.black_scholes import black_scholes
from py_vollib.black_scholes.implied_volatility import implied_volatility


@xl_func
def black_scholes_func(s, k, t, r, is_call, volatility):
    """
    https://github.com/vollib/py_vollib
    py_lets_be_rational and lets_be_rational, py_vollib and vollib are almost identical.
    Each is orders of magnitude faster than traditional implied volatility calculation libraries,
    thanks to the algorithms developed by Peter Jäckel.
    However, py_vollib, without Numba installed, is about an order of magnitude slower than vollib.
    Numba helps to mitigate this speed gap considerably.
    """
    flag = 'c' if is_call else 'p'
    return black_scholes(flag, s, k, t, r, volatility)


@xl_func
def implied_volatility_func(op_rice, s, k, t, r, is_call, realized_volatility):
    flag = 'c' if is_call else 'p'
    return implied_volatility(op_rice, s, k, t, r, flag)
